#!/bin/bash

#Written to run with docker image python:3.7

#Assumptions:
#This shell script assumes that several python scripts will exist, and that they will be in the same folder from which this script is run
#get_mirrors_list.py
#get_source_urls.py
#combine_mirror_data_sources.py


#Arguments:
#First argument is assumed to be a file path to the freedesktop-sdk repo. (Or a period / ".")
#If the file path doesn't lead to the freedesktop-sdk repo, the script fails
#If a dot is supplied as the first argument, then the script looks for the repo in the current directory, and clones from gitlab if it doesn't already exist.
#Arguments starting with a dash are passed as flags to the final script (combine_mirror_data_sources.py). These must all come before the branch names.
#Other arguments are the names of branches to inspect.

#To do:
#Add option whether to clone-if-necessary

source_dir=$(pwd)

if [ $1 == "." ];
then
	if [ -d "./freedesktop-sdk" ]
	then
		echo -e "\e[32mFreeDesktop-SDK repository found. Inspecting Repository.\e[0m"
	else
		echo -e "\e[32mFreeDesktop-SDK repository not found. Cloning.\e[0m"
		git clone "https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git"
	fi
	repo_dir="./freedesktop-sdk"
else
	if [[ $1 != *"freedesktop-sdk/" ]] && [[ $1 != *"freedesktop-sdk" ]]; 
	then 
		echo -e "\e[31mFirst argument should be the path to the freedesktop-sdk repository on the local machine - or a period/dot.\nUnable to locate repository, exiting $0\e[0m"
		exit 1
	fi
	if [ -d "$1" ]
	then
		echo -e "\e[32mFreeDesktop-SDK repository found. Inspecting Repository.\e[0m"
		repo_dir=$1
	else
		echo -e "\e[31mUnable to locate repository, exiting $0\e[0m"
	fi
fi

shift

while [[ $1 == "-"* ]];
do
	flag_options="$flag_options$1 "
	shift
done


cd "$repo_dir"
repo_dir=$(pwd)
current_branch="$(git symbolic-ref --short HEAD 2>/dev/null)" 
if [ "$current_branch" != "" ]
then
	echo -e "\e[32mRecording current branch:\e[0m $current_branch"
else
        echo "Repository seems to be in detached head state"
        current_branch=$(git log -n1 --pretty=format:%h)
        echo -e "\e[32mRecording current commit:\e[0m $current_branch"
fi

if [[ $# -eq 0 ]]
then
	declare -a branchlist=($current_branch)
else
	declare -a branchlist=$@
fi


append_flag=""
for branchname in $branchlist
do
	if ! git checkout "$branchname"
	then
		echo -e "\e[31mCouldn't checkout all branches. Returning to original git branch and exiting $0\e[0m"
		git checkout $current_branch
		exit 1
	fi
	if ! python3 "$source_dir/get_source_urls.py" "$repo_dir" -o "$source_dir/list_of_freedesktop-sdk_source_urls.csv" -l $branchname $append_flag
	then
		echo -e "\e[31mError encountered. Returning to original git branch and exiting $0\e[0m"
		git checkout $current_branch
		exit 1
	fi
	append_flag="-a"
	shift
done
echo -e "\e[32mReturning to original branch\e[0m"
git checkout $current_branch


cd $source_dir
if ! python3 get_mirrors_list.py 
then
	echo -e "\e[31mExiting $0\e[0m"
	exit 1
fi
python3 combine_mirror_data_sources.py $flag_options || exit 1
